﻿
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MDatos : MonoBehaviour
{
    public Toggle ingLeve, ingModerado, ingGrave;
    public Toggle vaAusencia, vaPresencia, vaSangrado;
    public Toggle asAusencia, asPresencia;
    public Dropdown pPortal;
    public Image Img;
    public static int Band=1;
    string[] Grave = new string[] { "2.5", "5", "7.5", "10"};
    string[] Moderado = new string[] { "3.7", "10", "11.2", "15" };
    //string[] mesesGrave = new string[] {"2 años 6 meses", " 5 años", "7 años 6 meses", "10 años"};
    //string[] mesesModerado = new string[] { "3 años 8 meses", "10 años", "11 años 2 meses", "15 años"};
    public static int contActivador = 0;
    public static int contAnimacion = 0;
    MsgToast message =new MsgToast();
    // Start is called before the first frame update
    void Start()
    {
        Img.enabled=false;
        
    }
    // Update is called once per frame
    void Update()
    {
        toggle();          
    }
    public void toggle()
    {
        if ((ingGrave.isOn || ingModerado.isOn || ingLeve.isOn) && (vaSangrado.isOn || vaPresencia.isOn || vaAusencia.isOn) &&(asAusencia.isOn || asPresencia.isOn) && (pPortal.value >= 0 && pPortal.value <= 14))
        {
            if (ingLeve.isOn && vaAusencia.isOn && pPortal.value >=0 && pPortal.value <= 4)
            {
                contActivador = 1;
            }
            if (ingLeve.isOn && asAusencia.isOn && vaAusencia.isOn && pPortal.value > 4 && pPortal.value <= 9 )
            {
                contActivador = 2;
                Band = 1;
                contAnimacion = 1;
            }
            if (ingLeve.isOn && asAusencia.isOn && vaPresencia.isOn && pPortal.value > 9 && pPortal.value <= 12 )
            {
                contActivador = 3;
                Band = 2;
                contAnimacion = 1;
            }
            if (ingLeve.isOn && asPresencia.isOn)
            {
                contActivador = 4;
                contAnimacion = 3;
            }
            if (ingLeve.isOn && vaSangrado.isOn)
            {
                contActivador = 5;
                contAnimacion = 4;
            }
            if (ingLeve.isOn && vaPresencia && asAusencia.isOn)
            {
                contActivador = 3;
                Band = 2;
                contAnimacion = 1;
            }
            if (ingModerado.isOn && asAusencia.isOn && vaAusencia.isOn && pPortal.value > 4 && pPortal.value <= 9 )
            {
                contActivador = 6;
                Band = 1;
                contAnimacion = 1;
            }
            if (ingModerado.isOn && asAusencia.isOn && vaPresencia.isOn && pPortal.value > 9 && pPortal.value <= 12 )
            {
                contActivador = 7;
                Band = 2;
                contAnimacion = 1;
            }
            if (ingModerado.isOn && asPresencia.isOn)
            {
                contActivador = 8;
                contAnimacion = 3;
            }
            if (ingModerado.isOn && vaSangrado.isOn)
            {
                contActivador = 9;
                contAnimacion = 4;
            }
            if (ingGrave.isOn && asAusencia.isOn && vaAusencia.isOn && pPortal.value > 4 && pPortal.value <= 9 )
            {
                contActivador = 10;
                Band = 1;
                contAnimacion = 1;
            }
            if (ingGrave.isOn && asAusencia.isOn && vaPresencia.isOn && pPortal.value > 9 && pPortal.value <= 12 )
            {
                contActivador = 11;
                Band = 2;
                contAnimacion = 1;
            }
            if (ingGrave.isOn && asPresencia.isOn)
            {
                contActivador = 12;
                contAnimacion = 3;
            }
            if (ingGrave.isOn && vaSangrado.isOn)
            {
                contActivador = 13;
                contAnimacion = 4;
            }
            Img.enabled = true;
        } 
        else
        {
            Img.enabled = false;
            contActivador =0;
        }

    } 
    public  void Acepta()
    {
        Debug.Log(contActivador);
        if(contActivador < 1 || contActivador > 13 && !Img.enabled)
        {
            message.DataGap();
        }
        else if (contActivador == 1)
        {
            
                SceneManager.LoadScene(0);
            
        }
        else
        {
            SceneManager.LoadScene(5);
        }
    }
    public void Cancela()
    {
        SceneManager.LoadScene(Rotacion.escena);
    }
    public static int getCont()
    {
        return contActivador;
    }
}
