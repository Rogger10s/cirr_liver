﻿using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.Video;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class VideoPlayerController : MonoBehaviour, IDragHandler, IPointerDownHandler 
{
 
 // Start is called before the first frame update
    [SerializeField]
    private VideoPlayer player;
    
    public Button vPause, vPlay;
    public Image btnImage;
    public TextMeshProUGUI dato;
    public Image barra;
    public GameObject aviso;
    string[] mesesGrave = new string[] {"2 años 6 meses", " 5 años", "7 años 6 meses", "10 años"};
    string[] mesesModerado = new string[] { "3 años 8 meses", "10 años", "11 años 2 meses", "15 años"};
    

    void Start()
    {
        btnImage = GameObject.Find("Play").GetComponent<Image>();
        btnImage.enabled = false;
        if (MDatos.getCont() >= 2 && MDatos.getCont() <= 5)
        {
            aviso.SetActive(false);
        }
        if (MDatos.getCont() == 6)
        {
            dato.text = mesesModerado[0];
        }
        if (MDatos.getCont() == 7)
        {
            dato.text = mesesModerado[1];
        }
        if (MDatos.getCont() == 8)
        {
            dato.text = mesesModerado[2];
        }
        if (MDatos.getCont() == 9)
        {
            dato.text = mesesModerado[3];
        }
        if (MDatos.getCont() == 10)
        {
            dato.text = mesesGrave[0];
        }
        if (MDatos.getCont() == 11)
        {
            dato.text = mesesGrave[1];
        }
        if (MDatos.getCont() == 12)
        {
            dato.text = mesesGrave[2];
        }
        if (MDatos.getCont() == 13)
        {
            dato.text = mesesGrave[3];
        }
    }
    void Update()
    {
        if (player.frameCount > 0)
        {
            barra.fillAmount = (float)player.frame / (float)player.frameCount;
            
        }
        if (barra.fillAmount >=0.99)
        {
            etapa();
        }
        
        
    }
    public void etapa()
    {
        if(MDatos.getCont() == 1)
        {
            SceneManager.LoadScene(0);
        }
        if(MDatos.getCont()==2 || MDatos.getCont()==3 || MDatos.getCont()==6 || MDatos.getCont()==7 || MDatos.getCont()==10 || MDatos.getCont()==11)
        {
            SceneManager.LoadScene(2);
        }
        if (MDatos.getCont()==4 || MDatos.getCont()==8 || MDatos.getCont()==12  )
        {
            SceneManager.LoadScene(3);
        }
        if (MDatos.getCont()==5 || MDatos.getCont()==9 || MDatos.getCont()==13  )
        {
            SceneManager.LoadScene(4);
        }
    }
    public void OnDrag(PointerEventData a)
    {
        TrySkip(a);
    }
    
    public void OnPointerDown(PointerEventData a)
    {
        TrySkip(a);
    }
    private void TrySkip(PointerEventData a)
    {
        Vector2 localPoint;
        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(
            barra.rectTransform, a.position, null, out localPoint))
            {
                float pt = Mathf.InverseLerp(barra.rectTransform.rect.xMin, barra.rectTransform.rect.xMax, localPoint.x);
                SkipTopPercent(pt);
            }
    }
    void SkipTopPercent(float pt)
    {
        var frame = player.frameCount * pt;
        player.frame =(long)frame;
    }




    private void Awake()
    {
        player = GetComponent<VideoPlayer>();
    }

    private void OnEnable()
    {
        player.prepareCompleted += OnPreparedCompleted;
    }
    private void OnPreparedCompleted(VideoPlayer source)
    {
        source.Play();
    }
    public void PlayCurrentClip()
    {
        if(!player.isPrepared)
        {
            player.Prepare();
        }
        else
        {
            player.Play();
        }   
    }
    public void PlayPlayer()
    {
        player.Play();
        btnImage.enabled = false;
        vPlay.enabled = false;
        vPause.enabled = true;
    }
    public void StopPlayer()
    {
        player.Stop();
        etapa();     
    }
    public void ResetPlayer()
    {
        player.Stop();
        player.time = 0f;
    }
    public void PausePlayer()
    {
        player.Pause();
        btnImage.enabled =true;
        vPause.enabled = false;
        vPlay.enabled = true;
    }
    public void SeekPlayer(float perc)
    {
        if (perc > 100)
        {
            perc=100;
        }
        else
        {
            perc=0;
        }
    }

}
