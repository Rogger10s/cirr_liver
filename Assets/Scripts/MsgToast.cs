﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MsgToast
{
    public void DataGap()
    {
        SSTools.ShowMessage ("Datos insuficientes",SSTools.Position.bottom,SSTools.Time.oneSecond);
    }
    public void LoadData()
    {
        SSTools.ShowMessage ("Cargar datos",SSTools.Position.bottom,SSTools.Time.oneSecond);
    }
}
