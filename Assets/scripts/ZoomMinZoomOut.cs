﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ZoomMinZoomOut : MonoBehaviour
{
    Camera MainCamer;
    Vector2 First, Second;
    float ZoomMdif, PrevDifer, CurDifer;
    [SerializeField]
    float ZoomSpeed = 0.1f;
    [SerializeField]
  
  
    void Start()
    {
        MainCamer= GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.touchCount == 2)
        {
            Touch FirstT = Input.GetTouch(0);
            Touch SecondT = Input.GetTouch(1);

            First = FirstT.position - FirstT.deltaPosition;
            Second = SecondT.position - SecondT.deltaPosition;

            PrevDifer = (First - Second).magnitude;
            CurDifer = (FirstT.position -  SecondT.position).magnitude;

            ZoomMdif = (FirstT.deltaPosition - SecondT.deltaPosition).magnitude * ZoomSpeed;

            if (PrevDifer > CurDifer)
                MainCamer.orthographicSize += ZoomMdif;
            if (PrevDifer < CurDifer)
                MainCamer.orthographicSize -= ZoomMdif;
        }
        MainCamer.orthographicSize = Mathf.Clamp (MainCamer.orthographicSize, 2f, 10f);
       
    }
}
