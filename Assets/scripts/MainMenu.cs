﻿using System.Data.SqlTypes;
using System.Transactions;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;
//[RequireComponent(typeof(TextMeshPro))]
public class MainMenu : MonoBehaviour
{
    public GameObject Panel, Panel1, Boton;
    //Rotacion r = new Rotacion();
  //  public Text Stage;
   // public TextMeshPro Stage;
    //public Text text;
    //public TextMesh tex;
    public TextMeshProUGUI Stage;
   
   
     string[] Etapas = new string[] { "Hígado", "Etapa 1", "Etapa 2", "Etapa 3", "Etapa 4" };

    void Start()
    {
       // Stage = GetComponet<TextMeshPro>();
        Panel.SetActive(false);
        Panel1.SetActive(false);
        if(SceneManager.GetActiveScene().buildIndex==2)
        {
            if (MDatos.Band!=2)
            {
                Stage.text=Etapas[1];
            }
            else
            {
                Stage.text=Etapas[2];
            }  
        }
        else if (SceneManager.GetActiveScene().buildIndex==3)
        {
            Stage.text = Etapas[3];
        }
        else if(SceneManager.GetActiveScene().buildIndex==4)
        {
            Stage.text = Etapas[4];
        }
        else
        {
            Stage.text = Etapas[0];
        }

    }

    void Update()
    {
    
        
    }
    public void DescriptionSint()
    {
        Rotacion.rot = 1;
        Boton.SetActive(false);
        
        if(MDatos.Band==2  && SceneManager.GetActiveScene().buildIndex == 2)
        {
            Desc1_2.SintDescrip2();
            Panel1.SetActive(false);
        }
        else
        {
            Panel.SetActive(true);
            Panel1.SetActive(false);
        }   
    }
    public void AceptSint()
    {
        Rotacion.rot = 0;
        Boton.SetActive(true);
        if (MDatos.Band == 2 && SceneManager.GetActiveScene().buildIndex == 2)
        {
            Desc1_2.AcepSint2();  
        }
        else
        {
            Panel.SetActive(false);
        }
        
    }
    public void AceptApp()
    {
        Rotacion.rot = 0;
        Boton.SetActive(true);
        Panel1.SetActive(false);
    }
    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    public void MainSintomas()
    {
        SceneManager.LoadScene(1);
    }
    public void AcercD()
    {
        Rotacion.rot = 1;
        Boton.SetActive(false);
        Panel.SetActive(false);
        Panel1.SetActive(true);  
        if(MDatos.Band == 2)
        {
            Desc1_2.Vent();
        } 
    }

}
