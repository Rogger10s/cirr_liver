﻿using System;
using System.Net.Mime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class MenuSintomas : MonoBehaviour
{
    // Start is called before the first frame update
    public Toggle Sint1, Sint2, Sint3, Sint4, Sint5, Sint6, Sint7, Sint8, Sint9, Sint10, Sint11, Sint12, Sint13, 
    Sint14, Sint15, Sint16, Sint17, Sint18;
    public Button Acept;
    public Image Img;
    
    int Dato, cont=0, cont2=0;
    public static int Band=1;

    MsgToast message =new MsgToast();
  //  public Color color = new Color(0.2F, 0.3F, 0.4F, 0.5F);
    void Start()
    {
     //   Dato = Rotacion.escena;
      //  Acept.enabled=false;
        Img.enabled=false;
        
        
    }

    // Update is called once per frame
    void Update()
    {
        if( Sint1.isOn && Sint17.isOn)
        {
            cont=1;
          //  Acept.enabled=true;
            Img.enabled=true;
            cont2=1;

        }
        else{
            
            if(cont2==1)
            {
                cont=0;
              //  Acept.enabled=false;
                Img.enabled=false;
            }
            
        }
        
        if(Sint18.isOn && Sint17.isOn || Sint9.isOn && Sint17.isOn || Sint9.isOn && Sint18.isOn )
        {
            cont=2;
          //  Acept.enabled=true;
            Img.enabled=true;
            cont2=2;
        }
        else
        {
            
            if(cont2==2)
            {
                cont=0;
          //      Acept.enabled=false;
                Img.enabled=false;
            }
        }
        if(Sint2.isOn && Sint9.isOn || Sint14.isOn && Sint9.isOn || Sint2.isOn && Sint14.isOn || 
        Sint2.isOn && Sint18.isOn || Sint14.isOn && Sint18.isOn || Sint9.isOn && Sint18.isOn || Sint14.isOn || Sint2.isOn && Sint1.isOn || 
        Sint9.isOn && Sint1.isOn|| Sint14.isOn && Sint1.isOn || Sint18.isOn && Sint1.isOn)
        {
            cont=3;
         //   Acept.enabled=true;
            Img.enabled=true;
            cont2=3;
        }
        else
        {          
            if(cont2==3)
            {
                cont=0;
          //      Acept.enabled=false;
                Img.enabled=false;
            }
        }
        if(Sint7.isOn && Sint15.isOn || Sint7.isOn && Sint3.isOn || Sint15.isOn && Sint3.isOn || 
                Sint7.isOn && Sint6.isOn || Sint15.isOn && Sint6.isOn || Sint3.isOn && Sint6.isOn ||
                Sint7.isOn && Sint13.isOn || Sint15.isOn && Sint13.isOn || Sint3.isOn && Sint13.isOn || Sint6.isOn && Sint13.isOn)
        {
            cont=4;
       //     Acept.enabled=true;
            Img.enabled=true;
            cont2=4;
        }
        else
        {
            
            if(cont2==4)
            {
                cont=0;
        //        Acept.enabled=false;
                Img.enabled=false;
            }
        }
        if(Sint11.isOn && Sint4.isOn|| Sint4.isOn && Sint5.isOn || Sint4.isOn && Sint10.isOn || Sint4.isOn && Sint12.isOn || 
                Sint4.isOn && Sint16.isOn || Sint11.isOn && Sint5.isOn || Sint11.isOn && Sint10.isOn || Sint11.isOn && Sint12.isOn ||
                Sint11.isOn && Sint16.isOn || Sint10.isOn && Sint5.isOn || Sint10.isOn && Sint12.isOn || Sint10.isOn && Sint16.isOn ||
                Sint5.isOn && Sint12.isOn || Sint5.isOn && Sint16.isOn || Sint12.isOn && Sint16.isOn || Sint8.isOn && Sint4.isOn || 
                Sint8.isOn && Sint11.isOn || Sint8.isOn && Sint5.isOn || Sint8.isOn && Sint10.isOn || Sint8.isOn && Sint12.isOn || 
                Sint8.isOn && Sint16.isOn || Sint11.isOn || Sint12.isOn) 
        {
            cont=5;
        //    Acept.enabled=true;
            Img.enabled=true;
            cont2=5;
        }
        else
        {
            if(cont2==5)
            {
        //        Acept.enabled=false;
                Img.enabled=false;
                cont=0;
            }
            
        }
        
    }
    
    public void Aceptar()
    { 
        Debug.Log("cosas");
        if(cont==1)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);   
        }
        if(cont==2)
        {
            SceneManager.LoadScene(2);
            Band=1;
        }
        if(cont==3)
        {
            SceneManager.LoadScene(2);
            Band=2;
        }
        if(cont==4)
        {
            SceneManager.LoadScene(3);
        }
        if(cont==5)
        {
            SceneManager.LoadScene(4);
        }
        if(cont < 1 || cont >5 && !Img.enabled)
        {
            message.DataGap();
        }

    }
    public void Cancelar()
    {
        SceneManager.LoadScene(Rotacion.escena);
    }
    
}
//sint8=debilidad muscular