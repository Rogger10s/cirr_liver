﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Rotacion : MonoBehaviour
{

    // Start is called before the first frame update
    public static int escena;
    public float SpeedR = 20;
    public static int rot ;
    void Start()
    {
        escena = SceneManager.GetActiveScene().buildIndex;
    
    }

    void Update()
    {
        
        if(Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Moved && getRot() == 0)
        {
            Vector2 Position = Input.GetTouch(0).deltaPosition;
        
            float x = Position.x * Mathf.Deg2Rad * SpeedR ;
            float y = Position.y * Mathf.Deg2Rad * SpeedR;
            transform.RotateAround(Vector3.zero, Vector3.up, -x );
            transform.RotateAround(Vector3.zero, Vector3.right, y);     
            
        }
        
    }
    public void setRot(int r)
    {
        rot = r;
    }
    public int getRot()
    { 
        return rot;
    }
}
