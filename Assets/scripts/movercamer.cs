﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class movercamer : MonoBehaviour
{
    Camera MainCamer;
    Vector2 First, Second;
    float ZoomMdif, PrevDifer, CurDifer;
    
    
  
    void Start()
    {
        MainCamer= GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.touchCount == 2)
        {
            Touch FirstT = Input.GetTouch(0);
            Touch SecondT = Input.GetTouch(1);

            First = FirstT.position - FirstT.deltaPosition;
            Second = SecondT.position - SecondT.deltaPosition;

            PrevDifer = (First - Second).magnitude;
            CurDifer = (FirstT.position -  SecondT.position).magnitude;

            ZoomMdif = PrevDifer - CurDifer;
        

            Camera.main.fieldOfView += ZoomMdif * 0.19f;
            Camera.main.fieldOfView = Mathf.Clamp(Camera.main.fieldOfView, 30.0f, 70.0f);
        }

        
    }
}
